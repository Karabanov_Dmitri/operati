import org.junit.Test;

import static org.junit.Assert.*;

public class operationTest {

    @Test
    public void main() {
        assertEquals(4, operation.increment(2, 2));
        assertEquals(-4, operation.increment(2, -2));
        assertEquals(-4, operation.increment(-2, 2));
        assertEquals(4, operation.increment(-2, -2));
        assertEquals(0, operation.increment(2, 0));
        assertEquals(0, operation.increment(0, 2));
        assertEquals(0, operation.increment(0, 0));
    }
}