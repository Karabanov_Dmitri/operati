import java.util.Scanner;

public class operation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("ввод 1 множетителя");
        int op1 = scanner.nextInt();
        System.out.println("ввод 2 множетителя");
        int op2 = scanner.nextInt();
        System.out.println("Ответ: " + increment(op1, op2));
    }

    public static int increment(int op1, int op2) {
        int result = 0;
        if (op1 == 0 && op2 == 0) {
            return 0;
        }
        if (op1 < 0 & op2 < 0) {
            op1 = -op1;
            op2 = -op2;
        }
        boolean znacMin = false;
        if (op1 < 0) {
            znacMin = true;
            op1 = -op1;
        }
        if (op2 < 0) {
            znacMin = true;
            op2 = -op2;
        }

        if (op1 >= op2) {
            int zam;
            zam = op1;
            op1 = op2;
            op2 = zam;
        }

        while (op1 != 0 && op2 != 0) {
            result += op2;
            op1--;
        }
        if (znacMin) {
            result = -result;
        }
        return result;
    }
}
